<?php

// Template Name:Homepage

?>


<?php get_header(); ?>

<div id="slider"></div>
<div id="slider_main">
<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>	<!-- SLIDE  -->
				<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" colour-theme="light-blue" >
					<!-- MAIN IMAGE -->
					<!-- <img src=""  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat"> -->
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class=" paragraph  customout"
						data-x="85"
						data-y="424"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 2"><p>NZAR Holdings is a proudly Sri Lankan company with wide-ranging experience and international pedigree. From our base in Colombo we work with customers, clients, agents and partners all over the world. You can find out more about what we do here and we welcome the opportunity to discuss how we can assist you going forwards.</p>
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin customout" 
						data-x="450" data-hoffset="100"
						data-y="bottom" data-voffset="0"
						data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="700"
						data-easing="Power4.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						style="z-index: 3"><img src="<?php echo get_template_directory_uri();?>/img/slider.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="heading tp-caption large_bold_grey skewfromrightshort customout"
						data-x="80"
						data-y="96"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 4">NZAR HOLDINGS
					</div>
				  </li>

				  <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" colour-theme="dark-blue" >
					<!-- MAIN IMAGE -->
					<!-- <img src=""  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat"> -->
					<!-- LAYERS -->

					<!-- LAYER NR. 1 -->
					<div class=" paragraph  customout"
						data-x="85"
						data-y="424"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1200"
						data-easing="Power4.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 2"><p>NZAR Holdings is a proudly Sri Lankan company with wide-ranging experience and international pedigree. From our base in Colombo we work with customers, clients, agents and partners all over the world. You can find out more about what we do here and we welcome the opportunity to discuss how we can assist you going forwards.</p>
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption customin customout" 
						data-x="450" data-hoffset="100"
						data-y="bottom" data-voffset="0"
						data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="700"
						data-easing="Power4.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						style="z-index: 3"><img src="<?php echo get_template_directory_uri();?>/img/slider2.png" alt="">
					</div>

					<!-- LAYER NR. 3 -->
					<div class="heading tp-caption large_bold_grey skewfromrightshort customout"
						data-x=""
						data-y="96"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						data-captionhidden="on"
						style="z-index: 4">NZAR Gems & Jewellery
					</div>
				  </li>
				</ul>
				<div class="tp-bannertimer"></div>
			</div>
		</div>

</div><!-- end of the slider main -->



<div class="intro">

<h1>Our Journey</h1>
<span>Partner for the wise</span>
<p>
NZAR Holdings (Pvt) Ltd. is a proudly Sri Lankan company working with customers, clients, agents and partners all over the world. 

It boasts a dedicated and professional staff who are truly service-oriented - not only catering to the needs of its clientele, but also providing valuable insight to whoever seeks its services.  Besides this, the secret of NZAR’s success lies in its humane and ethical approach towards all, and it often rises to the occasion to help the needy in whatever manner possible, without solely ‘devouring profits’ in its business dealings.

After first establishing goodwill among its clientele, and then building up a solid reputation amongst satisfied third-parties who have reaped the benefits of its extensive products and services, the Company has been increasingly successful, re-investing profits for the further expansion and development of the Company.

‘Success breeds success’

The name of NZAR has now spread to all corners of the globe, whilst it continues to operate its business diligently, methodically and in a transparent manner.  Today it is ranked as one of the top diversified businesses in Sri Lanka.

NZAR is proud to have been appointed as both an accredited agent/representative of, and supplier to, a large number of leading companies all over the world. And with new customers, clients, agents and partners joining them every day, the business continues to go from strength-to-strength.


</p>

</div>


<div class="nzar_catgories">

<div class="catergory">
	<div class="box moreleft">
			<div class="background">
				<img src="<?php echo get_template_directory_uri();?>/img/cat_bg.png" alt="">
			</div>
			<div class="curve_box">

				<div class="text">
				<h1>Nzar tea</h1>
				<span>Supply,trading,Export & Consultancy </span>
				</div>
				<div class="number">
					<div class='circle'>
						<span>1</span>

					</div>
				</div>

			</div>



	</div>

		<div class="box">
			
			<div class="curve_box reverse-box">

				<div class="number number_reverse">
					<div class='circle'>
						<span>1</span>

					</div>
				</div>

				<div class="text reverse">
				<h1>Nzar tea</h1>
				<span>Supply,trading,Export & Consultancy </span>
				</div>
				

			</div>

			<div class="background background_reverse">
				<img src="<?php echo get_template_directory_uri();?>/img/cat_bg_1.png" alt="">
			</div>



	</div>

<div class="box moreleft">
			<div class="background">
				<img src="<?php echo get_template_directory_uri();?>/img/cat_bg.png" alt="">
			</div>
			<div class="curve_box">

				<div class="text">
				<h1>Nzar tea</h1>
				<span>Supply,trading,Export & Consultancy </span>
				</div>
				<div class="number">
					<div class='circle'>
						<span>1</span>

					</div>
				</div>

			</div>



	</div>

<div class="box">
			
			<div class="curve_box reverse-box">

				<div class="number number_reverse">
					<div class='circle'>
						<span>1</span>

					</div>
				</div>

				<div class="text reverse">
				<h1>Nzar tea</h1>
				<span>Supply,trading,Export & Consultancy </span>
				</div>
				

			</div>

			<div class="background background_reverse">
				<img src="<?php echo get_template_directory_uri();?>/img/cat_bg_1.png" alt="">
			</div>



	</div>

</div>

</div>




</div><!-- .content -->

<div id='partners'>
	<div class="content">
		<div class="headding-2">
			<h2>Our partners</h2>
		</div>
		<div class='col-md-4'>
				<img src="<?php echo get_template_directory_uri(); ?>/img/edb.svg">
			</div>
			<div class='col-md-4'>
				<img src="<?php echo get_template_directory_uri(); ?>/img/icc.svg">
			</div>
			<div class='col-md-4'>
				<img src="<?php echo get_template_directory_uri(); ?>/img/tea-board.svg">
			</div>
			<div class='col-md-4'>
				<img src="<?php echo get_template_directory_uri(); ?>/img/exporters-association.svg">
			</div>
	</div><!-- .content -->
</div>

<div id='feeds'>
	<div class="content">
	<div id='home-news' class='col-md-6'>
		<h2>Latest news <span><i></i></span></h2>
			<div class='news-cont'>
				<div class='news-cont-child'>
					<div class='news-img'>
						<img src="<?php echo get_template_directory_uri(); ?>/img/gem.jpg">
					</div>

					<div class='news-info'>
						<h4>Lorem Ipsum</h4>
						<span><i class='icon-eye'>0</i></span>
						<span><i class='icon-chat'>0</i></span>
						
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, Lorem Ipsum is simply </p>
						
					</div>
				</div>
				<div class='news-btn' ><a href="">Read more</a></div>
				
			</div>
			<div class='news-cont'>
				<div class='news-cont-child'>
					<div class='news-img'>
						<img src="<?php echo get_template_directory_uri(); ?>/img/gem.jpg">
					</div>

					<div class='news-info'>
						<h4>Lorem Ipsum</h4>
						<span><i class='icon-eye'>0</i></span>
						<span><i class='icon-chat'>0</i></span>
						
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, Lorem Ipsum is simply </p>
						
					</div>
				</div>
				<div class='news-btn' ><a href="">Read more</a></div>
				
			</div>
	</div>

	<div id='testamonials' class='col-md-6'>
		<h2>What people say <span><i></i></span></h2>
		<div class='test-slider-div'>
			<ul class="bxslider">
			  <li>
				<div class="box-header">
					<div class='slider-img'>
					<img src="<?php echo get_template_directory_uri(); ?>/img/ed-glauser-picture.jpg">
					</div>
					<div class='slider-header-name'>
						<h4>Imran khan</h4>
						<small>Lorem Ipsum is simply</small>
					</div>
				</div>
				<div class="box-body">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				</div>
			  </li>
			  <li>
				<div class="box-header">
					<div class='slider-img'>
					<img src="<?php echo get_template_directory_uri(); ?>/img/ed-glauser-picture.jpg">
					</div>
					<div class='slider-header-name'>
						<h4>Imran khan</h4>
						<small>Lorem Ipsum is simply</small>
					</div>
				</div>
				<div class="box-body">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				</div>
			  </li>
			</ul>
		</div>
	</div>
	</div><!-- .content -->
</div>

<script type="text/javascript">

$(document).ready(function(){
  $('.bxslider').bxSlider({
	auto: true,
	speed: 2000
  });
});

</script>
<?php get_footer(); ?>

