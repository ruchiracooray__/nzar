<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package nzar
 */
?>

	

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="content">
			<div class='col-md-4'>
				<h6>Info for</h6>
				<ul>
					<li><a href="">Developers</a></li>
					<li><a href="">IT professionals</a></li>
					<li><a href="">Small business</a></li>
					<li><a href="">Enterprise</a></li>
					<li><a href="">Students</a></li>
				</ul>

				<h6>Info for</h6>
				<ul>
					<li><a href="">NZAR Holdings</a></li>
					<li><a href="">Windows downloads</a></li>
					<li><a href="">Windows themes</a></li>
					<li><a href="">Wallpapers</a></li>
					<li><a href="">Free antivirus</a></li>
					<li><a href="">Photo Gallery</a></li>
					<li><a href="">Movie Maker</a></li>
					<li><a href="">Language packs</a></li>
				</ul>
			</div><!--end of section 1-->

			<div class='col-md-4'>
				<h6>Info for</h6>
				<ul>
					<li><a href="">searches</a></li>
					<li><a href="">NZAR Holdings</a></li>
					<li><a href="">Microsoft account</a></li>
					<li><a href="">Start screen</a></li>
					<li><a href="">Apps</a></li>
					<li><a href="">Windows Store</a></li>
					<li><a href="">Internet Explorer 11</a></li>
					<li><a href="">Windows 8.1 Update</a></li>
					<li><a href="">Free downloads</a></li>
					<li><a href="">Internet browser</a></li>
					<li><a href="">Tablet and PC</a></li>
					<li><a href="">Cortana</a></li>
				</ul>
			</div><!--end of section 2-->

			<div class='col-md-4'>
				<h6>Products</h6>
				<ul>
					<li><a href="">Lorem Ipsum</a></li>
					<li><a href="">NZAR RT 8.1</a></li>
					<li><a href="">Internet Explorer</a></li>
					<li><a href="">NZAR RT 8.17</a></li>
					<li><a href="">NZAR RT 8.1</a></li>
					<li><a href="">NZAR RT 8.1 Mail</a></li>
					<li><a href="">Hotmail</a></li>
				</ul>
				<h6>Latest info</h6>
				<ul>
					<li><a href="">The Windows Blog</a></li>
					<li><a href="">Windows newsletter</a></li>
					<li><a href="">Windows Product Guide</a></li>
				</ul>
			</div><!--end of section 3-->

			<div class='col-md-4'>
				<h6>About us</h6>
				<p>NZAR Holdings is a proudly Sri Lankan company with wide-ranging experience and international pedigree. From our base in Colombo we work with customers, clients, agents and partners all over the world. You can find out more about what we do here and we welcome the opportunity to discuss how we can assist you going forwards.</p>

				<h6>Newsletter</h6>
				<p class='newsletter'>Signup for the monthly newsletter to get usefull information for your business</p>
				<input type='text'>
				<button type='submit'>Sign up</button>

				<h6>Newsletter</h6>
				<ul class="social">
					<li><a class="icon-twitter" href=""></a></li>
					<li><a class="icon-facebook" href=""></a></li>
					<li><a class="icon-google-plus" href=""></a></li>
				</ul>
			</div><!--end of section 4-->

			<div id="footer-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-small.svg">
			</div>
		</div>
	</footer><!-- #colophon -->

		<script type="text/javascript">

				var revapi;

				jQuery(document).ready(function() {

					   revapi = jQuery('.tp-banner').revolution(
						{
							delay:6000,
							startwidth:1170,
							startheight:500,
							hideThumbs:10

						});



					var slide_count =jQuery('.tp-banner').length + 1;
					var count=0;


					revapi.bind("revolution.slide.onchange",function (e,data) {


						count++;
						if (count == slide_count){
							count=0;
						}
						var colour_theme = jQuery('.tp-banner-container ul li').attr('colour-theme');
						var second = jQuery('.tp-banner-container ul li').eq(count).attr('colour-theme');
						jQuery("#slider").removeClass();
						jQuery("#slider").addClass(second);
					});

					

					// bind to button click
				

				});	//ready

			</script>

<?php wp_footer(); ?>

</body>
</html>
